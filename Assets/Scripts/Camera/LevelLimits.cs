﻿using UnityEngine;

namespace AB.Cameras
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class LevelLimits : MonoBehaviour
    {
        public float LeftLimit { get; private set; }
        public float RightLimit { get; private set; }
        public float BottomLimit { get; private set; }
        public float TopLimit { get; private set; }

        public BoxCollider2D Collider;


        /// <summary>
        /// On awake, fills the public variables with the level's limits
        /// </summary>
        private void Awake()
        {
            LeftLimit = Collider.bounds.min.x;
            RightLimit = Collider.bounds.max.x;
            BottomLimit = Collider.bounds.min.y;
            TopLimit = Collider.bounds.max.y;
        }
    }
}