﻿using UnityEngine;

namespace AB.Cameras
{
    public class CameraController : MonoBehaviour
    {
        [Header("Objects")] //
        public Transform Player;
        public Camera Camera;
        public LevelLimits LevelLimits;

        [Header("Player Follow")] //
        public bool UseFixedUpdate;

        // Minimal distance that triggers look ahead
        [Header("Movement Options")] //
        public float HorizontalOffset = 3f;
        public float VerticalOffset = 3f;
        public float SmoothTime = 0.5f;

        // Level bounds
        private float _xMin;
        private float _xMax;
        private float _yMin;
        private float _yMax;

        // Camera follow variable storage
        private Vector3 _currentOffset;
        private Vector3 _lastTargetPosition;
        private Vector3 _currentVelocity;


        /// <summary>
        /// Use this for initialization
        /// </summary>
        private void Start()
        {
            GetLevelLimits();
        }


        /// <summary>
        /// Runs after update
        /// </summary>
        private void LateUpdate()
        {
            if (!UseFixedUpdate) UpdateCameraPosition();
        }


        /// <summary>
        /// Runs after update
        /// </summary>
        private void FixedUpdate()
        {
            if (UseFixedUpdate) UpdateCameraPosition();
        }


        /// <summary>
        /// Smooth Follow the player
        /// </summary>
        private void UpdateCameraPosition()
        {
            if (Player == null) return;

            // if the player has moved since last update
            var xMoveDelta = (Player.position - _lastTargetPosition).x;
            var yMoveDelta = (Player.position - _lastTargetPosition).y;

            // Only offset the axes if the player is moving faster than the trigger
            var currentXOffset = Mathf.Abs(xMoveDelta) > 0.05f ? HorizontalOffset * Mathf.Sign(xMoveDelta) : 0f;
            var currentYOffset = Mathf.Abs(yMoveDelta) > 0.05f ? VerticalOffset * Mathf.Sign(yMoveDelta) : 0f;

            // Sort out the offset
            var newPos = new Vector3(currentXOffset, currentYOffset, transform.position.z);
            _currentOffset = Vector3.SmoothDamp(_currentOffset, newPos, ref _currentVelocity, SmoothTime);

            // Get the position with offset
            var newCameraPosition = new Vector3(
                Player.position.x + _currentOffset.x,
                Player.position.y + _currentOffset.y,
                transform.position.z
            );

            // Clamp to level boundaries
            transform.position = new Vector3(
                Mathf.Clamp(newCameraPosition.x, _xMin, _xMax),
                Mathf.Clamp(newCameraPosition.y, _yMin, _yMax),
                newCameraPosition.z
            );

            // Save the last position
            _lastTargetPosition = Player.position;
        }


        /// <summary>
        /// Get the boundaries of the level from a big kinematic box collider
        /// </summary>
        private void GetLevelLimits()
        {
            var cameraHeight = Camera.orthographicSize * 2f;
            var cameraWidth = cameraHeight * Camera.aspect;

            _xMin = LevelLimits.LeftLimit + cameraWidth / 2;
            _xMax = LevelLimits.RightLimit - cameraWidth / 2;
            _yMin = LevelLimits.BottomLimit + cameraHeight / 2;
            _yMax = LevelLimits.TopLimit - cameraHeight / 2;
        }
    }
}