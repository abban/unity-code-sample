﻿using AB.Models;
using UnityEngine.SceneManagement;

namespace AB.Events
{
    public class PlaySignal
    {
        public SaveModel SaveModel { get; }
        
        public PlaySignal(SaveModel saveModel)
        {
            SaveModel = saveModel;
        }
    }
    
    public class LoadSceneSignal
    {
        public string Scene { get; }
        public LoadSceneMode Mode { get; }

        public LoadSceneSignal(string scene, LoadSceneMode mode = LoadSceneMode.Single)
        {
            Scene = scene;
            Mode = mode;
        }
    }


    public class SceneLoadedSignal
    {
        public Scene Scene { get; }
        public LoadSceneMode Mode { get; }

        public SceneLoadedSignal(Scene scene, LoadSceneMode mode)
        {
            Scene = scene;
            Mode = mode;
        }
    }
}
