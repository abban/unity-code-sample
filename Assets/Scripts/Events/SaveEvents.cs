﻿using AB.Models;

namespace AB.Events
{
    public class SaveSignal
    {
    }

    public class DeleteSignal
    {
        public string Key { get; }

        public DeleteSignal(string key)
        {
            Key = key;
        }
    }

    public class SaveModelSignal
    {
        public SaveModel SaveModel { get; }

        public SaveModelSignal(SaveModel saveModel)
        {
            SaveModel = saveModel;
        }
    }


    public class ShowSavePanelSignal
    {   
    }
    
    
    public class HideSavePanelSignal
    {
    }
}