﻿namespace AB.Events
{
	public class SetNotificationSignal
	{
		public string Content { get; }

		public SetNotificationSignal(string content)
		{
			Content = content;
		}
	}
}