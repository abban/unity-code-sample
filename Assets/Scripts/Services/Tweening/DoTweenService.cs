﻿using DG.Tweening;
using UnityEngine;
using AB.Services.Tweening.Interfaces;

namespace AB.Services.Tweening
{
    public class DoTweenService : ITweenService
    {
        /// <summary>
        /// Fade a canvas group in a co-routine and wait for the animation to finish
        /// </summary>
        /// <param name="canvasGroup"></param>
        /// <param name="to"></param>
        /// <param name="time"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        public YieldInstruction Fade(CanvasGroup canvasGroup, float to, float time, bool update)
        {
            var tween = canvasGroup.DOFade(to, time).SetEase(Ease.InOutExpo).SetUpdate(update);
            return tween.WaitForCompletion();
        }


        /// <summary>
        /// Scale a transform in a co-routine and wait for it to finish
        /// </summary>
        /// <param name="transform"></param>
        /// <param name="to"></param>
        /// <param name="time"></param>
        /// <param name="update"></param>
        /// <returns></returns>
        public YieldInstruction Scale(Transform transform, float to, float time, bool update)
        {
            var tween = transform.DOScale(to, time).SetEase(Ease.InOutExpo).SetUpdate(update);
            return tween.WaitForCompletion();
        }
    }
}