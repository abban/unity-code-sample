﻿using UnityEngine;

namespace AB.Services.Tweening.Interfaces
{
    public delegate void TweenDelegate();
    
    public interface ITweenService
    {
        YieldInstruction Fade(CanvasGroup canvasGroup, float to, float time, bool update = false);
        YieldInstruction Scale(Transform transform, float to, float time, bool update = false);
    }
}