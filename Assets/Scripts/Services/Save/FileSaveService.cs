﻿using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using UnityEngine;
using AB.Models;
using AB.Services.Save.Interfaces;

namespace AB.Services.Save
{
	public class FileSaveService : ISaveService
	{
		/// <inheritdoc />
		public SaveModel Load(string save, SaveModel into)
		{
			if (!Exists(save)) return into;
			
			var binaryFormatter = new BinaryFormatter();
			var file = File.Open(Application.persistentDataPath + save, FileMode.Open);
			var saveData = binaryFormatter.Deserialize(file) as SaveModel;
			file.Close();
			return saveData;
		}

		
		/// <inheritdoc />
		public void Save(string save, SaveModel data)
		{
			var binaryFormatter = new BinaryFormatter();
			var file = File.Create(Application.persistentDataPath + save);
			binaryFormatter.Serialize(file, data);
			file.Close();
		}

		
		/// <inheritdoc />
		public void Delete(string save)
		{
			File.Delete(Application.persistentDataPath + save);
		}

		
		/// <inheritdoc />
		public bool Exists(string save)
		{
			return File.Exists(Application.persistentDataPath + save);
		}
	}
}