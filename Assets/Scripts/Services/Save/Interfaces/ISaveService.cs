﻿using AB.Models;

namespace AB.Services.Save.Interfaces
{
	/// <summary>
	/// This interface is to be used when saving data to a file
	/// The reason it uses a non interfaced SaveModel is because SaveModel is technically a singleton
	/// and any attributes or variables contained in it would need to be added to its interface
	/// in order for the save service to be able to serialise those into a file
	/// </summary>
	public interface ISaveService
	{
		/// <summary>
		/// Load a file into an object
		/// </summary>
		/// <param name="save"></param>
		/// <param name="into"></param>
		/// <returns></returns>
		SaveModel Load(string save, SaveModel into);
		
		
		/// <summary>
		/// Save an object as a file
		/// </summary>
		/// <param name="save"></param>
		/// <param name="data"></param>
		void Save(string save, SaveModel data);
		
		
		/// <summary>
		/// Delete the file
		/// </summary>
		/// <param name="save"></param>
		void Delete(string save);
		
		
		/// <summary>
		/// Check if a file exists
		/// </summary>
		/// <returns></returns>
		bool Exists(string save);
	}
}