﻿using Zenject;
using UnityEngine.SceneManagement;
using AB.Events;
using AB.Models;
using AB.UI.Transition.Interfaces;

namespace AB.Controllers
{
	public class SceneController : IInitializable
	{
		private Session _session;
		private ITransitionController _transitionController;

		private string _sceneToLoad;

		
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="session"></param>
		/// <param name="transitionController"></param>
		public SceneController(
			Session session,
			ITransitionController transitionController)
		{
			_session = session;
			_transitionController = transitionController;
		}

		
		/// <summary>
		/// When this object is initialised
		/// </summary>
		public void Initialize()
		{
			SceneManager.sceneLoaded += OnSceneLoaded;
		}

		
		/// <summary>
		/// Event handler for loading a scene
		/// </summary>
		/// <param name="loadSceneSignal"></param>
		public void OnLoadScene(LoadSceneSignal loadSceneSignal)
		{
			TransitionToScene(loadSceneSignal.Scene);
		}

		
		/// <summary>
		/// Event handler for selecting a save to play
		/// </summary>
		/// <param name="playSignal"></param>
		public void OnPlay(PlaySignal playSignal)
		{
			_session.CurrentSave = playSignal.SaveModel;
			TransitionToScene(playSignal.SaveModel.CurrentRoom);
		}


		/// <summary>
		/// Show the transition then wait for it to be visible
		/// </summary>
		/// <param name="scene"></param>
		private void TransitionToScene(string scene)
		{
			_sceneToLoad = scene;
			_transitionController.ShowTransition();
		}

		
		/// <summary>
		/// Callback on the transition visible event
		/// </summary>
		public void OnTransitionVisible()
		{
			if (_sceneToLoad == null) return;
			SceneManager.LoadSceneAsync(_sceneToLoad);
			_sceneToLoad = null;
		}


		/// <summary>
		/// Callback on a scene loaded event
		/// </summary>
		/// <param name="scene"></param>
		/// <param name="mode"></param>
		private void OnSceneLoaded(Scene scene, LoadSceneMode mode)
		{
			_transitionController.HideTransition();
		}
	}
}