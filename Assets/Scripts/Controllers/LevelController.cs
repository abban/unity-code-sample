﻿using System;
using Zenject;
using AB.Characters;
using AB.Events;
using AB.Models;

namespace AB.Controllers
{
	public class LevelController : IInitializable
	{
		private Session _session;
		private SignalBus _signalBus;
		
		private CharacterFacade _character;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="session"></param>
		/// <param name="signalBus"></param>
		public LevelController(
			Session session,
			SignalBus signalBus)
		{
			_session = session;
			_signalBus = signalBus;
		}
		
		
		/// <summary>
		/// When this object is initialised
		/// </summary>
		/// <exception cref="NotImplementedException"></exception>
		public void Initialize()
		{
			_signalBus.Fire(new SetNotificationSignal("WASD to move, W to use doors, S to pick up items"));
		}
	}
}