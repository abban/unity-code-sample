﻿using AB.Events;
using AB.UI.Transition.Interfaces;

namespace AB.Controllers
{
	public class MainMenuController
	{
		private ITransitionController _transitionController;
		
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="transitionController"></param>
		public MainMenuController(
			ITransitionController transitionController)
		{
			_transitionController = transitionController;
		}
		
		
		/// <summary>
		/// Scene loaded callback
		/// </summary>
		public void OnSceneLoaded(SceneLoadedSignal sceneLoadedSignal)
		{
			_transitionController.HideTransition();
		}
	}
}