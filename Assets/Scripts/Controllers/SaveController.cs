﻿using System;
using System.Collections.Generic;
using Zenject;
using AB.Events;
using AB.Models;
using AB.Services.Save.Interfaces;

namespace AB.Saving
{
	public class SaveController : IInitializable
	{	
		private Settings _settings;
		private Session _session;
		private ISaveService _saveService;
		
		
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="settings"></param>
		/// <param name="session"></param>
		/// <param name="saveService"></param>
		public SaveController(
			Settings settings,
			Session session,
			ISaveService saveService)
		{
			_settings = settings;
			_session = session;
			_saveService = saveService;
		}
		
		
		/// <summary>
		/// When this object is initialised
		/// </summary>
		public void Initialize()
		{
			foreach (var save in _settings.Saves)
			{
				save.Reset();
				_saveService.Load(save.Key, save);
			}
		}

			
		/// <summary>
		/// Save a model
		/// </summary>
		/// <param name="saveModel"></param>
		private void Save(SaveModel saveModel)
		{
			_saveService.Save(saveModel.Key, saveModel);
		}

		
		/// <summary>
		/// Callback on a delete event
		/// </summary>
		/// <param name="deleteSignal"></param>
		public void OnDeleteSignal(DeleteSignal deleteSignal)
		{
			_saveService.Delete(deleteSignal.Key);
		}


		/// <summary>
		/// Callback on a generic save event
		/// </summary>
		public void OnSaveSignal()
		{
			if(_session == null) return;
			if (_session.CurrentSave == null) return;
			Save(_session.CurrentSave);
		}
		
		
		/// <summary>
		/// Callback on a a save event on a specific model
		/// </summary>
		/// <param name="saveModelSignal"></param>
		public void OnSaveModelSignal(SaveModelSignal saveModelSignal)
		{
			Save(saveModelSignal.SaveModel);
		}

		
		[Serializable]
		public class Settings
		{
			public List<SaveModel> Saves = new List<SaveModel>();
		}
	}
}