﻿namespace AB.Input
{
    public enum Direction
    {
        None,
        Up,
        Right,
        Down,
        Left
    }
}