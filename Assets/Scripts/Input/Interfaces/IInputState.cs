﻿using UnityEngine;

namespace AB.Input.Interfaces
{
	public interface IInputState
	{
		/// <summary>
		/// Input on X-Axis
		/// </summary>
		float XAxis { get; set; }
		
		/// <summary>
		/// Input on Y-Axis
		/// </summary>
		float YAxis { get; set; }
		
		/// <summary>
		/// If the run button is held
		/// </summary>
		bool Run { get; set; }
		
		/// <summary>
		/// If the jump button is held
		/// </summary>
		bool Jump { get; set; }

		/// <summary>
		/// When the input first happens
		/// </summary>
		bool PointerDown { get; set; }
		
		/// <summary>
		/// When the input is held
		/// </summary>
		bool PointerHeld { get; set; }
		
		/// <summary>
		/// When the input ends
		/// </summary>
		bool PointerUp { get; set; }
		
		/// <summary>
		/// If this input was a tap
		/// </summary>
		bool PointerTapped { get; set; }
		
		/// <summary>
		/// If this input was a swipe
		/// </summary>
		bool PointerSwiped { get; set; }
		
		/// <summary>
		/// Enum with direction of swipe
		/// </summary>
		Direction PointerSwipeDirection { get; set; }
		
		/// <summary>
		/// World position of input
		/// </summary>
		Vector2 PointerPosition { get; set;  }
		
		/// <summary>
		/// Call this to reset the states to default 
		/// </summary>
		void Reset();
	}
}