﻿/*
 * This is an example of an interface I would use for touchscreens with the Fingers addon.
 */

/*using UnityEngine;
using UnityEngine.UI;
using DigitalRubyShared;
using Zenject;
using AB.Input.Interfaces;

namespace AB.Input
{
    public class TouchHandler : IInitializable, ITickable
    {
        private IInputState _gestureState;
        private TapGestureRecognizer _tapGesture;
        private SwipeGestureRecognizer _swipeGesture;
        private LongPressGestureRecognizer _longPressGesture;


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="gestureState"></param>
        public GestureHandler(IInputState gestureState)
        {
	        _gestureState = gestureState;
        }

		
	    /// <summary>
	    /// Use this for initialisation
	    /// </summary>
        public void Initialize()
        {
	        // Tell the gestures plugin to ignore a few types of elements 
	        FingersScript.Instance.ComponentTypesToIgnorePassThrough.Add(typeof(Image));
	        // FingersScript.Instance.ComponentTypesToIgnorePassThrough.Add(typeof(SkeletonGraphic));

	        CreateTapGesture();
	        CreateSwipeGesture();
	        CreateLongPressGesture();
        }
	    
	    
	    /// <summary>
	    /// Runs once a frame
	    /// </summary>
	    public void Tick()
	    {
		    _gestureState.Reset();
	    }
        
        
        /// <summary>
		/// Initialise a new tap gesture and hook it to a listener
		/// </summary>
		private void CreateTapGesture()
		{
			_tapGesture = new TapGestureRecognizer();
			_tapGesture.StateUpdated += TapGestureCallback;
			FingersScript.Instance.AddGesture(_tapGesture);
		}

		
		/// <summary>
		/// Initialise a new swipe gesture and hook it to a listener
		/// </summary>
		private void CreateSwipeGesture()
		{
			_swipeGesture = new SwipeGestureRecognizer
			{
				Direction = SwipeGestureRecognizerDirection.Any,
				MinimumDistanceUnits = 0.2f,
				MinimumSpeedUnits = 1f,
				DirectionThreshold = 1f
			};

			_swipeGesture.StateUpdated += SwipeGestureCallback;
			FingersScript.Instance.AddGesture(_swipeGesture);
		}
		
		
		/// <summary>
		/// Initialise a new long press gesture and hook it to a listener
		/// </summary>
		private void CreateLongPressGesture()
		{
			_longPressGesture = new LongPressGestureRecognizer
			{
				MaximumNumberOfTouchesToTrack = 1,
				MinimumDurationSeconds = 0.3f
			};
			
			_longPressGesture.StateUpdated += LongPressGestureCallback;
			FingersScript.Instance.AddGesture(_longPressGesture);
		}
		
		
		/// <summary>
		/// Tap gesture listener
		/// </summary>
		/// <param name="gesture"></param>
		private void TapGestureCallback(UnityEngine.XR.WSA.Input.GestureRecognizer gesture)
		{	
			if (gesture.State != GestureRecognizerState.Ended) return;
			
			_gestureState.PointerPosition = new Vector2(gesture.FocusX, gesture.FocusY);
			_gestureState.PointerTapped = true;
		}
		
		
		/// <summary>
		/// Swipe gesture listener
		/// </summary>
		/// <param name="gesture"></param>
		private void SwipeGestureCallback(UnityEngine.XR.WSA.Input.GestureRecognizer gesture)
		{	
			if (gesture.State != GestureRecognizerState.Ended) return;
			
			Direction currentDirection;
			
			if (Mathf.Abs(gesture.DistanceY) > Mathf.Abs(gesture.DistanceX))
			{
				// Vertical
				currentDirection = gesture.DistanceY > 0 ? Direction.Up : Direction.Down;
			}
			else
			{
				// horizontal
				currentDirection = gesture.DistanceX > 0 ? Direction.Right : Direction.Left;
			}

			_gestureState.PointerSwiped = true;
			_gestureState.PointerSwipeDirection = currentDirection;
		}
		
		
		/// <summary>
		/// Long press gesture listener
		/// </summary>
		/// <param name="gesture"></param>
		private void LongPressGestureCallback(UnityEngine.XR.WSA.Input.GestureRecognizer gesture)
		{
			_gestureState.PointerPosition = new Vector2(gesture.FocusX, gesture.FocusY);
			
			switch (gesture.State)
			{
				case GestureRecognizerState.Began:
					_gestureState.PointerDown = true;
					break;
				case GestureRecognizerState.Executing:
					_gestureState.PointerHeld = true;
					break;
				case GestureRecognizerState.Ended:
					_gestureState.PointerUp = true;
					break;
			}
		}
    }
}*/