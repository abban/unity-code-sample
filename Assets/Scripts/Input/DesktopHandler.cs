﻿using UnityEngine;
using Zenject;
using AB.Input.Interfaces;

namespace AB.Input
{
    public class DesktopHandler : ITickable
    {
        private float _delay = 0.3f;
        private float _distance = 0.3f;
        private float _clickTime;
        private Vector3 _startPosition = Vector3.zero;

        private IInputState _inputState;

        public DesktopHandler(IInputState inputState)
        {
            _inputState = inputState;
        }


        /// <summary>
        /// Look for mouse input on every frame
        /// Don't change the order of the methods
        /// </summary>
        public void Tick()
        {
            _inputState.Reset();
            
            CheckAxes();
            CheckButtons();
            CheckPointerDown();
            CheckPointerUp();
            CheckPointerHold();
        }


        /// <summary>
        /// Looks for directional input
        /// </summary>
        private void CheckAxes()
        {
            _inputState.XAxis = UnityEngine.Input.GetAxis("Horizontal");
            _inputState.YAxis = UnityEngine.Input.GetAxis("Vertical");
        }

        
        /// <summary>
        /// Look for button input
        /// </summary>
        private void CheckButtons()
        {
            _inputState.Jump = UnityEngine.Input.GetButtonDown("Jump");
            _inputState.Run = UnityEngine.Input.GetButtonDown("Run");
        }


        /// <summary>
        /// Check a mouse down
        /// </summary>
        private void CheckPointerDown()
        {
            if (!UnityEngine.Input.GetMouseButtonDown(0)) return;
            if (Camera.main == null) return;

            _clickTime = Time.time;
            _startPosition = UnityEngine.Input.mousePosition;

            _inputState.PointerPosition = Camera.main.ScreenToWorldPoint(_startPosition);
            _inputState.PointerDown = true;
        }


        /// <summary>
        /// Check for mouse up
        /// </summary>
        private void CheckPointerUp()
        {
            if (!UnityEngine.Input.GetMouseButtonUp(0)) return;
            if (Camera.main == null) return;
            
            _inputState.PointerPosition = Camera.main.ScreenToWorldPoint(UnityEngine.Input.mousePosition);
            _inputState.PointerUp = true;
            
            if (!(Time.time - _delay <= _clickTime)) return;
            _inputState.PointerTapped = true;
        }


        /// <summary>
        /// Check for mouse held
        /// </summary>
        private void CheckPointerHold()
        {
            if (!UnityEngine.Input.GetMouseButton(0)) return;
            if (Camera.main == null) return;
            
            var position = UnityEngine.Input.mousePosition;
            
            // If time intent hasn't been reached or the mouse hasn't moved enough then it ain't a hold
            if (!(Time.time - _clickTime > _delay) && (_startPosition - position).magnitude < _distance) return;

            _inputState.PointerPosition = Camera.main.ScreenToWorldPoint(position);
            _inputState.PointerHeld = true;
        }
    }
}