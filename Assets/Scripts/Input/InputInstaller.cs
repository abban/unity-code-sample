using Zenject;
using AB.Input.Interfaces;

namespace AB.Input
{
    public class InputInstaller : MonoInstaller<InputInstaller>
    {
        public override void InstallBindings()
        {
            Container.Bind<IInputState>()
                .To<InputState>()
                .AsSingle();
            
            Container.BindInterfacesAndSelfTo<DesktopHandler>()
                .AsSingle()
                .NonLazy();
        }
    }
}