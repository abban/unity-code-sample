﻿using UnityEngine;
using AB.Input.Interfaces;

namespace AB.Input
{
    public class InputState : IInputState
    {
        public float XAxis { get; set; }
        public float YAxis { get; set; }
        public bool Run { get; set; }
        public bool Jump { get; set; }
        public bool PointerDown { get; set; }
        public bool PointerHeld { get; set; }
        public bool PointerUp { get; set; }
        public bool PointerTapped { get; set; }
        public bool PointerSwiped { get; set; }
        public Direction PointerSwipeDirection { get; set; }
        public Vector2 PointerPosition { get; set; }

        public void Reset()
        {
            XAxis = 0;
            YAxis = 0;
            Run = false;
            Jump = false;
            PointerDown = false;
            PointerHeld = false;
            PointerUp = false;
            PointerTapped = false;
            PointerSwiped = false;
            PointerSwipeDirection = Direction.None;
            PointerPosition = Vector2.zero;
        }
    }
}