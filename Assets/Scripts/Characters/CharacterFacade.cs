﻿using UnityEngine;
using Zenject;
using AB.Characters.Interfaces;

namespace AB.Characters
{
	public class CharacterFacade : MonoBehaviour
	{
		private ICharacterTriggerHandler _triggerHandler;


		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="triggerHandler"></param>
		[Inject]
		public void Construct(
			ICharacterTriggerHandler triggerHandler)
		{
			_triggerHandler = triggerHandler;
		}

		
		/// <summary>
		/// Character enters a trigger
		/// </summary>
		/// <param name="other"></param>
		private void OnTriggerEnter2D(Collider2D other)
		{
			_triggerHandler.OnTriggerEnter2D(other);
		}
		
		
		/// <summary>
		/// Character leaves a trigger
		/// </summary>
		/// <param name="other"></param>
		private void OnTriggerExit2D(Collider2D other)
		{
			_triggerHandler.OnTriggerExit2D(other);
		}
	}
}