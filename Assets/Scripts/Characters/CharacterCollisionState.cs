﻿using AB.Characters.Interfaces;

namespace AB.Characters
{
    public class CharacterCollisionState : ICharacterCollisionState
    {
        public bool Right { get; set; }
        public bool Left { get; set; }
        public bool Above { get; set; }
        public bool Below { get; set; }
        public bool BecameGroundedThisFrame { get; set; }
        public bool WasGroundedLastFrame { get; set; }
        public bool MovingDownSlope { get; set; }
        public float SlopeAngle { get; set; }
        public float RaycastXPoint { get; set; }
        public float RaycastYPoint { get; set; }


        /// <inheritdoc />
        public bool HasCollision => Below || Right || Left || Above;

        
        /// <inheritdoc />
        public bool HasXCollision => Right || Left;

        
        /// <inheritdoc />
        public bool HasYCollision => Below || Above;
        

        /// <inheritdoc />
        public bool IsGrounded => Below;

        
        /// <inheritdoc />
        public void Reset()
        {
            WasGroundedLastFrame = IsGrounded;
            Right = false;
            Left = false;
            Above = false;
            Below = false;
            BecameGroundedThisFrame = false;
            MovingDownSlope = false;
            SlopeAngle = 0f;
            RaycastXPoint = 0f;
            RaycastYPoint = 0f;
        }


        /// <inheritdoc cref="ICharacterCollisionState" />
        public override string ToString()
        {
            return
                $"[CharacterCollisionState2D] r: {Right}, l: {Left}, a: {Above}, b: {Below}, movingDownSlope: {MovingDownSlope}, angle: {SlopeAngle}, wasGroundedLastFrame: {WasGroundedLastFrame}, becameGroundedThisFrame: {BecameGroundedThisFrame}";
        }
    }
}