﻿using Zenject;
using UnityEngine;
using AB.Input.Interfaces;
using AB.Characters.Interfaces;
using AB.Events;
using AB.Level.InventorySystem;

namespace AB.Characters
{
    public class CharacterController2D : ILateTickable
    {
        private Character _character;
        private IInputState _input;
        private ICharacterCollisionState _collisionState;
        private ICharacterTriggerState _triggerState;
        private ICharacterCollisionHandler _collisionHandler;
        private ICharacterMoveHandler _moveHandler;
        private Inventory _inventory;
        private SignalBus _signalBus;


        private Vector2 _velocity;


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="character"></param>
        /// <param name="input"></param>
        /// <param name="collisionState"></param>
        /// <param name="triggerState"></param>
        /// <param name="collisionHandler"></param>
        /// <param name="moveHandler"></param>
        /// <param name="inventory"></param>
        /// <param name="signalBus"></param>
        public CharacterController2D(
            Character character,
            IInputState input,
            ICharacterCollisionState collisionState,
            ICharacterTriggerState triggerState,
            ICharacterCollisionHandler collisionHandler,
            ICharacterMoveHandler moveHandler,
            Inventory inventory,
            SignalBus signalBus)
        {
            _character = character;
            _input = input;
            _collisionState = collisionState;
            _triggerState = triggerState;
            _collisionHandler = collisionHandler;
            _moveHandler = moveHandler;
            _inventory = inventory;
            _signalBus = signalBus;
        }

        
        /// <summary>
        /// Late update
        /// </summary>
        public void LateTick()
        {
            Movement();
            Items();
            Doors();
        }

        
        /// <summary>
        /// Handle movement
        /// </summary>
        private void Movement()
        {
            if (_collisionState.IsGrounded) _velocity.y = 0;

            // Add gravity and x-axis
            _velocity.x = _input.XAxis * _character.Speed;

            // we can only jump whilst grounded
            if (_collisionState.IsGrounded && _input.Jump)
            {
                _velocity.y = Mathf.Sqrt(2f * _character.JumpHeight * -_character.Gravity);
            }
            
            // Add gravity
            _velocity.y += _character.Gravity * Time.deltaTime;
            
            // Multiply by time
            _velocity *= Time.deltaTime;
            
            // Fire rays, check for collisions
            _collisionHandler.Check(_velocity);

            // Move the character
            _moveHandler.Move(_velocity);
            
            // Store the old velocity
            _velocity = _character.Velocity;
        }

        
        /// <summary>
        /// Handle item pickups
        /// </summary>
        private void Items()
        {
            if (_input.YAxis >= 0) return;
            if (_triggerState.Item == null) return;

            _signalBus.Fire(new SetNotificationSignal("Found: " + _triggerState.Item.Item.Name));
            
            _inventory.AddItem(_triggerState.Item.Item);
            _triggerState.Item.Kill();
        }

        
        /// <summary>
        /// Handle door entry
        /// </summary>
        private void Doors()
        {
            if (_input.YAxis <= 0) return;
            if (_triggerState.Door == null) return;

            var requiredItem = _triggerState.Door.RequiredItem;

            if (requiredItem != null && !_inventory.HasItem(requiredItem))
            {
                _signalBus.Fire(new SetNotificationSignal("Need: " + requiredItem.Name));
                return;
            }
            
            _inventory.RemoveItem(requiredItem);
            _signalBus.Fire(new LoadSceneSignal(_triggerState.Door.Room.SceneName));
        }
    }
}