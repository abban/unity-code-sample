﻿using System;
using UnityEngine;

namespace AB.Characters
{
    public class Character
    {
        private Settings _settings;
        
        public Character(Settings settings)
        {
            _settings = settings;
        }

        public Vector2 Velocity { get; private set; }

        public Transform Transform => _settings.Transform;
        public Rigidbody2D Rigidbody2D => _settings.Rigidbody2D;
        public BoxCollider2D BoxCollider2D => _settings.BoxCollider2D;
        public LayerMask GroundMask => _settings.GroundMask;
        public LayerMask TriggerMask => _settings.TriggerMask;
        public float JumpHeight => _settings.JumpHeight;
        public float Speed => _settings.Speed;
        public float Gravity => _settings.Gravity;
        public float SkinWidth => _settings.SkinWidth;


        public void Move(Vector3 delta)
        {
            _settings.Transform.Translate(delta, Space.World);
            
            if (Time.deltaTime > 0f) Velocity = delta / Time.deltaTime;
        }


        [Serializable]
        public class Settings
        {
            public Transform Transform;
            public Rigidbody2D Rigidbody2D;
            public BoxCollider2D BoxCollider2D;
            public LayerMask GroundMask;
            public LayerMask TriggerMask;
            
            [Range(0, 10)] public float JumpHeight = 3f;
            [Range(0, 10)] public float Speed = 8f;
            [Range(-100, 100)] public float Gravity = -84f;
            [Range(0.001f, 0.3f)] public float SkinWidth = 0.02f;
        }
    }
}