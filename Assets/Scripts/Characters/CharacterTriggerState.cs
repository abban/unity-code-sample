﻿using AB.Characters.Interfaces;
using AB.Level;

namespace AB.Characters
{
	public class CharacterTriggerState : ICharacterTriggerState
	{
		public Door Door { get; set; }
		public WorldItem Item { get; set; }
	}
}