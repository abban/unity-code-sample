﻿using UnityEngine;
using AB.Characters.Interfaces;

namespace AB.Characters
{
	public class CharacterMoveHandler : ICharacterMoveHandler
	{
		private Character _character;
		private ICharacterCollisionState _collisionState;
		
		private float SkinWidth => _character.SkinWidth;
		
		
		public CharacterMoveHandler(
			Character character,
			ICharacterCollisionState collisionState)
		{
			_character = character;
			_collisionState = collisionState;
		}
		
		
		public void Move(Vector3 delta)
		{			
			XMovement(ref delta);
			YMovement(ref delta);
			_character.Move(delta);
		}
		
		
		private void XMovement(ref Vector3 delta)
		{
			if (!_collisionState.HasXCollision) return;
			
			var isGoingRight = delta.x > 0;
			
			// Colliding so force the delta to the collision point
			delta.x = _collisionState.RaycastXPoint;
			
			// Then add or subtract the skin depth
			if (isGoingRight)
			{
				delta.x -= SkinWidth;
			}
			else
			{
				delta.x += SkinWidth;
			}
		}
		
		
		private void YMovement(ref Vector3 delta)
		{
			if (!_collisionState.HasYCollision) return;
			
			var isGoingUp = delta.y > 0;
			
			// Colliding so force the delta to the collision point
			delta.y = _collisionState.RaycastYPoint;

			// Then add or subtract the skin depth
			if (isGoingUp)
			{
				delta.y -= SkinWidth;
			}
			else
			{
				delta.y += SkinWidth;
			}
		}


		private void SlopeModifier(ref Vector3 deltaMovement)
		{
			
		}
	}
}