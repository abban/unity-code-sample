using UnityEngine;
using Zenject;

namespace AB.Characters
{
    public class CharacterInstaller : MonoInstaller<CharacterInstaller>
    {
        [SerializeField] private Character.Settings _settings;
        [SerializeField] private CharacterCollisionHandler.Settings _collisionSettings;
        
        public override void InstallBindings()
        {
            Container.Bind<Character>()
                .AsSingle()
                .WithArguments(_settings);
            
            Container.BindInterfacesTo<CharacterCollisionHandler>()
                .AsSingle()
                .WithArguments(_collisionSettings);

            Container.BindInterfacesTo<CharacterTriggerHandler>()
                .AsSingle();

            Container.BindInterfacesTo<CharacterCollisionState>()
                .AsSingle();

            Container.BindInterfacesTo<CharacterTriggerState>()
                .AsSingle();
            
            Container.BindInterfacesTo<CharacterMoveHandler>()
                .AsSingle();

            Container.BindInterfacesTo<CharacterController2D>()
                .AsSingle()
                .NonLazy();
        }
    }
}