﻿using System.Collections.Generic;
using UnityEngine;
using AB.Characters.Interfaces;
using AB.Level;
using Zenject;

namespace AB.Characters
{
	public class CharacterTriggerHandler : ICharacterTriggerHandler, IInitializable
	{
		private Character _character;
		private ICharacterTriggerState _triggerState;

		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="character"></param>
		/// <param name="triggerState"></param>
		public CharacterTriggerHandler(
			Character character,
			ICharacterTriggerState triggerState)
		{
			_character = character;
			_triggerState = triggerState;
		}
		
		
		/// <summary>
		/// When this object is initialised
		/// </summary>
		public void Initialize()
		{
			_triggerState.Item = null;
			_triggerState.Door = null;
		}

		
		/// <summary>
		/// Handle trigger entered
		/// </summary>
		/// <param name="other"></param>
		public void OnTriggerEnter2D(Collider2D other)
		{
			if (!InLayer(other.gameObject.layer)) return;

			switch (other.tag)
			{
				case "Door":
					AddDoor(other.gameObject.GetComponent<Door>());
					break;
				case "Item":
					AddItem(other.gameObject.GetComponent<WorldItem>());
					break;
			}
		}
		
		
		/// <summary>
		/// Handle trigger left
		/// </summary>
		/// <param name="other"></param>
		public void OnTriggerExit2D(Collider2D other)
		{
			if (!InLayer(other.gameObject.layer)) return;
			
			switch (other.tag)
			{
				case "Door":
					RemoveDoor(other.gameObject.GetComponent<Door>());
					break;
				case "Item":
					RemoveItem(other.gameObject.GetComponent<WorldItem>());
					break;
			}
		}

		
		/// <summary>
		/// Add a door
		/// </summary>
		/// <param name="door"></param>
		private void AddDoor(Door door)
		{
			if (door == null) return;
			if (_triggerState.Door == door) return;
			_triggerState.Door = door;
		}

		
		/// <summary>
		/// Remove a door
		/// </summary>
		/// <param name="door"></param>
		private void RemoveDoor(Door door)
		{
			if (door == null) return;
			if (_triggerState.Door != door) return;
			_triggerState.Door = null;
		}

		
		/// <summary>
		/// Add an item
		/// </summary>
		/// <param name="item"></param>
		private void AddItem(WorldItem item)
		{
			if (item == null) return;
			if (_triggerState.Item == item) return;
			_triggerState.Item = item;
		}
		
		
		/// <summary>
		/// Remove an item
		/// </summary>
		/// <param name="item"></param>
		private void RemoveItem(WorldItem item)
		{
			if (item == null) return;
			if (_triggerState.Item != item) return;
			_triggerState.Item = null;
		}

		
		/// <summary>
		/// Check a layer is in a layer mask
		/// </summary>
		/// <param name="layer"></param>
		/// <returns></returns>
		private bool InLayer(int layer)
		{
			return _character.TriggerMask == (_character.TriggerMask | (1 << layer));
		}
	}
}