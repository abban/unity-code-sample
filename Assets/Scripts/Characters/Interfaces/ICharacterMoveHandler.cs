﻿using UnityEngine;

namespace AB.Characters.Interfaces
{
    public interface ICharacterMoveHandler
    {
        void Move(Vector3 delta);
    }
}