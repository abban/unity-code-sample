﻿using UnityEngine;

namespace AB.Characters.Interfaces
{
	public interface ICharacterTriggerHandler
	{
		void OnTriggerEnter2D(Collider2D other);
		void OnTriggerExit2D(Collider2D other);
	}
}
