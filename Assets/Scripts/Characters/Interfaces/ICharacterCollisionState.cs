﻿using UnityEngine;

namespace AB.Characters.Interfaces
{
	public interface ICharacterCollisionState
	{
		bool Right { get; set; }
		bool Left { get; set; }
		bool Above { get; set; }
		bool Below { get; set; }
		bool BecameGroundedThisFrame { get; set; }
		bool WasGroundedLastFrame { get; set; }
		bool MovingDownSlope { get; set; }
		float SlopeAngle { get; set; }
		float RaycastXPoint { get; set; }
		float RaycastYPoint { get; set; }


		/// <summary>
		/// Check if this character is colliding with anything
		/// </summary>
		bool HasCollision { get; }

		
		/// <summary>
		/// Check if this character has an X Axis Collision
		/// </summary>
		bool HasXCollision { get; }
		
		
		/// <summary>
		/// Check if this character has a Y Axis Collision
		/// </summary>
		bool HasYCollision { get; }


		/// <summary>
		/// Check if this character is on the ground
		/// </summary>
		bool IsGrounded { get; }


		/// <summary>
		/// Reset
		/// </summary>
		void Reset();


		/// <summary>
		/// For debugging
		/// </summary>
		/// <returns></returns>
		string ToString();
	}
}