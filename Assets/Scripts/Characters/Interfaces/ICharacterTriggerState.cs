﻿using AB.Level;

namespace AB.Characters.Interfaces
{
	/// <summary>
	/// Not using interfaces here as this demo is taking too long to put together
	/// </summary>
	public interface ICharacterTriggerState
	{
		Door Door { get; set; }
		WorldItem Item { get; set; }
	}
}