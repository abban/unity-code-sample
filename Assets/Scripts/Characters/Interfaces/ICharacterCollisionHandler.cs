﻿using UnityEngine;

namespace AB.Characters.Interfaces
{
	public interface ICharacterCollisionHandler
	{
		void Check(Vector3 delta);
	}
}