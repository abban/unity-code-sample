﻿using System;
using UnityEngine;
using Zenject;
using AB.Characters.Interfaces;

namespace AB.Characters
{
    /// <summary>
    /// Raycasts and fills the Collision State
    /// </summary>
    public class CharacterCollisionHandler : ICharacterCollisionHandler, IInitializable
    {
        struct CharacterRaycastOrigins
        {
            public Vector3 TopLeft;
            public Vector3 BottomRight;
            public Vector3 BottomLeft;
        }

        private Character _character;
        private Settings _settings;
        private ICharacterCollisionState _collisionState;
        private CharacterRaycastOrigins _raycastOrigins;

        private Vector2 _raySpacing;
        
        private Vector2 LocalScale => _character.Transform.localScale;
        private float SkinWidth => _character.SkinWidth;
        private int HorizontalRays => _settings.HorizontalRays;
        private int VerticalRays => _settings.VerticalRays;
        private BoxCollider2D BoxCollider2D => _character.BoxCollider2D;
        private LayerMask GroundMask => _character.GroundMask;

        private const float SkinWidthFudgeFactor = 0.001f;

        
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="character"></param>
        /// <param name="settings"></param>
        /// <param name="collisionState"></param>
        public CharacterCollisionHandler(
            Character character,
            Settings settings,
            ICharacterCollisionState collisionState)
        {
            _character = character;
            _settings = settings;
            _collisionState = collisionState;
        }

        
        /// <summary>
        /// Initialise this object
        /// </summary>
        public void Initialize()
        {
            RecalculateDistanceBetweenRays();
        }

        
        /// <summary>
        /// Check for collisions
        /// </summary>
        /// <param name="delta"></param>
        public void Check(Vector3 delta)
        {
            _collisionState.Reset();
            
            // clear our state
            // _raycastHitsThisFrame.Clear();
            // _isGoingUpSlope = false;
            
            ResetRaycastOrigins();

            if (Mathf.Abs(delta.x) > 0.001) CheckXAxis(delta);
            if (Mathf.Abs(delta.y) > 0.001) CheckYAxis(delta);
        }
        
        
        /// <summary>
        /// Check for collisions in the X-Axis
        /// </summary>
        /// <param name="delta"></param>
        private void CheckXAxis(Vector3 delta)
        {
            var isGoingRight = delta.x > 0;
            var rayDistance = Mathf.Abs(delta.x) + SkinWidth;
            var rayDirection = isGoingRight ? Vector2.right : Vector2.left;
            var initialRayOrigin = isGoingRight ? _raycastOrigins.BottomRight : _raycastOrigins.BottomLeft;

            for (var i = 0; i < HorizontalRays; i++)
            {
                var ray = new Vector2(initialRayOrigin.x, initialRayOrigin.y + i * _raySpacing.y);
                DrawRay(ray, rayDirection * rayDistance, Color.red);
                
                var raycastHit = Physics2D.Raycast(ray, rayDirection, rayDistance, GroundMask);

                if (!raycastHit) continue;
                
                // Store the raycast hit point
                _collisionState.RaycastXPoint = raycastHit.point.x - ray.x;
                rayDistance = Mathf.Abs(_collisionState.RaycastXPoint);

                // Set collision states
                if (isGoingRight)
                {
                    _collisionState.Right = true;
                }
                else
                {
                    _collisionState.Left = true;
                }

                //_raycastHitsThisFrame.Add(_raycastHit);

                // we add a small fudge factor for the float operations here. if our rayDistance is smaller
                // than the width + fudge bail out because we have a direct impact
                if (rayDistance < SkinWidth + SkinWidthFudgeFactor) break;
            }
        }
        
        
        /// <summary>
        /// Check for collisions in the X-Axis
        /// </summary>
        /// <param name="delta"></param>
        private void CheckYAxis(Vector3 delta)
        {
            var isGoingUp = delta.y > 0;
            var rayDistance = Mathf.Abs(delta.y) + SkinWidth;
            
            var rayDirection = isGoingUp ? Vector2.up : -Vector2.up;
            var initialRayOrigin = isGoingUp ? _raycastOrigins.TopLeft : _raycastOrigins.BottomLeft;

            // apply our horizontal deltaMovement here so that we do our raycast from the actual position we would be in if we had moved
            //initialRayOrigin.x += delta.x;

            for (var i = 0; i < VerticalRays; i++)
            {
                var ray = new Vector2(initialRayOrigin.x + i * _raySpacing.x, initialRayOrigin.y);
                DrawRay(ray, rayDirection * rayDistance, Color.red);
                
                var raycastHit = Physics2D.Raycast(ray, rayDirection, rayDistance, GroundMask);
                if (!raycastHit) continue;
                
                // Store the raycast hit point
                _collisionState.RaycastYPoint = raycastHit.point.y - ray.y;
                rayDistance = Mathf.Abs(_collisionState.RaycastYPoint);
                
                // Store the collision
                if (isGoingUp)
                {
                    _collisionState.Above = true;
                }
                else
                {
                    _collisionState.Below = true;
                }

                // this is a hack to deal with the top of slopes. if we walk up a slope and reach the apex we can get in a situation
                // where our ray gets a hit that is less then skinWidth causing us to be ungrounded the next frame due to residual velocity.
                // if (!isGoingUp && deltaMovement.y > 0.00001f) _isGoingUpSlope = true;

                // we add a small fudge factor for the float operations here. if our rayDistance is smaller
                // than the width + fudge bail out because we have a direct impact
                if (rayDistance < SkinWidth + SkinWidthFudgeFactor) break;
            }
        }


        /// <summary>
        /// Reset the raycast origin positions to the current position of the character
        /// </summary>
        private void ResetRaycastOrigins()
        {
            // our raycasts need to be fired from the bounds inset by the skinWidth
            var modifiedBounds = BoxCollider2D.bounds;
            modifiedBounds.Expand(-2f * SkinWidth);

            _raycastOrigins.TopLeft = new Vector2(modifiedBounds.min.x, modifiedBounds.max.y);
            _raycastOrigins.BottomRight = new Vector2(modifiedBounds.max.x, modifiedBounds.min.y);
            _raycastOrigins.BottomLeft = modifiedBounds.min;
        }

        
        /// <summary>
        /// Draw a ray
        /// </summary>
        /// <param name="start"></param>
        /// <param name="dir"></param>
        /// <param name="color"></param>
        private static void DrawRay(Vector3 start, Vector3 dir, Color color)
        {
            Debug.DrawRay(start, dir, color);
        }


        /// <summary>
        /// This calculates the distance that needs to be between the rays being fired horizontally and vertically
        /// </summary>
        private void RecalculateDistanceBetweenRays()
        {
            var xSize = BoxCollider2D.size.x * Mathf.Abs(LocalScale.x) - 2f * SkinWidth;
            var ySize = BoxCollider2D.size.y * Mathf.Abs(LocalScale.y) - 2f * SkinWidth;
            
            _raySpacing = new Vector2(
                xSize / (VerticalRays - 1),
                ySize / (HorizontalRays - 1)
            );
        }


        [Serializable]
        public class Settings
        {
            [Range(2, 20)] public int HorizontalRays = 8;
            [Range(2, 20)] public int VerticalRays = 4;
        }
    }
}