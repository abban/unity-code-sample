﻿using TMPro;
using UnityEngine;
using AB.Events;

namespace AB.UI.Level
{
	public class Notification : MonoBehaviour
	{
		public TextMeshProUGUI Text;
		
		public void OnSetNotificationSignal(SetNotificationSignal setNotificationSignal)
		{
			Text.text = setNotificationSignal.Content;
		}
	}
}