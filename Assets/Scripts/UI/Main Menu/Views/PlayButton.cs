﻿using AB.Events;
using AB.Models;
using UnityEngine;
using UnityEngine.EventSystems;
using Zenject;

namespace AB.UI.MainMenu.Views
{
	public class PlayButton : MonoBehaviour, IPointerClickHandler
	{
		public SaveModel Save;
		private SignalBus _signalBus;
		
		[Inject]
		public void Construct(SignalBus signalBus)
		{
			_signalBus = signalBus;
		}

		public void OnPointerClick(PointerEventData eventData)
		{
			_signalBus.Fire(new PlaySignal(Save));
		}
	}
}