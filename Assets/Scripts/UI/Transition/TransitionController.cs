﻿using System;
using UnityEngine;
using AB.UI.Transition.Interfaces;

namespace AB.UI.Transition
{	
	public class TransitionController : ITransitionController
	{
		private ITransition _transition;

		public TransitionController(ITransition transition)
		{
			_transition = transition;
		}


		public void ShowTransition()
		{ 
			_transition.Show();
		}
		
		
		public void HideTransition()
		{
			_transition.Hide();
		}

		[Serializable]
		public class Settings
		{
			public GameObject Transition;
		}
	}
}