﻿using System.Collections;
using UnityEngine;
using Zenject;
using AB.UI.Transition.Interfaces;
using AB.Services.Tweening.Interfaces;

namespace AB.UI.Transition.Views
{
    public class Transition : MonoBehaviour, ITransition
    {
        public CanvasGroup CanvasGroup;
        private SignalBus _signalBus;
        private ITweenService _tweenService;

        private bool _initialised;


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="signalBus"></param>
        /// <param name="tweenService"></param>
        [Inject]
        public void Constructor(
            SignalBus signalBus,
            ITweenService tweenService)
        {
            _signalBus = signalBus;
            _tweenService = tweenService;
        }

        
        /// <summary>
        /// Initialise
        /// </summary>
        private void Start()
        {
            CanvasGroup.alpha = 0;
            CanvasGroup.interactable = false;
            CanvasGroup.blocksRaycasts = false;
        }
        
        
        /// <summary>
        /// Show this transition
        /// </summary>
        public void Show()
        {
            StartCoroutine(ShowTransition());
        }


        /// <summary>
        /// Hide this transition    
        /// </summary>
        public void Hide()
        {
            StartCoroutine(HideTransition());
        }
        
        
        /// <summary>
        /// Animate the transition in
        /// </summary>
        /// <returns></returns>
        private IEnumerator ShowTransition()
        {
            CanvasGroup.interactable = true;
            CanvasGroup.blocksRaycasts = true;            
            yield return _tweenService.Fade(CanvasGroup, 1f, 0.2f);
            _signalBus.Fire(new TransitionVisibleSignal());
        }

        
        /// <summary>
        /// Animate the transition out
        /// </summary>
        /// <returns></returns>
        private IEnumerator HideTransition()
        {
            yield return _tweenService.Fade(CanvasGroup, 0f, 0.2f);
            CanvasGroup.interactable = false;
            CanvasGroup.blocksRaycasts = false;
        }
    }
}