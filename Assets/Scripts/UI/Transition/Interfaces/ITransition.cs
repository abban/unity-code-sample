﻿namespace AB.UI.Transition.Interfaces
{
	public interface ITransition
	{
		void Show();
		void Hide();
	}
}