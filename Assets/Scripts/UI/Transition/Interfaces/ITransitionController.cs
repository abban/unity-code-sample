﻿namespace AB.UI.Transition.Interfaces
{
	public interface ITransitionController
	{
		void ShowTransition();
		void HideTransition();
	}
}