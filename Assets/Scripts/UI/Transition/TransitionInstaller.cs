using UnityEngine;
using Zenject;

namespace AB.UI.Transition
{
    public class TransitionInstaller : MonoInstaller<TransitionInstaller>
    {
        [SerializeField] private TransitionController.Settings _settings;
        
        public override void InstallBindings()
        {
            SignalBusInstaller.Install(Container);
            
            Container.DeclareSignal<ShowTransitionSignal>();
            Container.DeclareSignal<HideTransitionSignal>();
            Container.DeclareSignal<TransitionVisibleSignal>().OptionalSubscriber();
            
            Container.BindInterfacesAndSelfTo<Views.Transition>()
                .FromComponentInNewPrefab(_settings.Transition)
                .AsSingle()
                .NonLazy();
            
            Container.BindInterfacesAndSelfTo<TransitionController>()
                .AsSingle()
                .NonLazy();
            
            Container.BindSignal<ShowTransitionSignal>()
                .ToMethod<TransitionController>(x => x.ShowTransition)
                .FromResolve();
            
            Container.BindSignal<HideTransitionSignal>()
                .ToMethod<TransitionController>(x => x.HideTransition)
                .FromResolve();
        }
    }
}