﻿using System;
using AB.Controllers;
using UnityEngine;
using Zenject;
using AB.Events;
using AB.Models;
using AB.Level.InventorySystem;
using AB.UI.Level;

namespace AB.Installers
{
	public class LevelInstaller : MonoInstaller<LevelInstaller>
	{
		[SerializeField] private Settings _settings;
		
		public override void InstallBindings()
		{
			Container.BindInterfacesTo<LevelController>()
				.AsSingle()
				.NonLazy();
			
			Container.Bind<Inventory>()
				.AsSingle()
				.NonLazy();

			Container.Bind<InventoryView>()
				.FromComponentInHierarchy()
				.AsSingle();
			
			Container.BindFactory<Item, Transform, GuiItem, GuiItem.Factory>()
				.FromFactory<GuiItemFactory>();

			Container.Bind<Notification>()
				.FromInstance(_settings.Notification)
				.AsSingle();
			
			Container.DeclareSignal<SetNotificationSignal>();

			Container.BindSignal<SetNotificationSignal>()
				.ToMethod<Notification>(x => x.OnSetNotificationSignal)
				.FromResolve();
		}
		
		[Serializable]
		public class Settings
		{
			public Notification Notification;
		}
	}
}