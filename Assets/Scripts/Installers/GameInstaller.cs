using System;
using UnityEngine;
using Zenject;
using AB.Controllers;
using AB.Events;
using AB.Models;
using AB.Saving;
using AB.Services.Save;
using AB.Services.Tweening;
using AB.UI.Transition;

namespace AB.Installers
{
    public class GameInstaller : MonoInstaller<GameInstaller>
    {
        [SerializeField] private Settings _settings;
        [SerializeField] private SaveController.Settings _saveSettings;
        
        public override void InstallBindings()
        {
            Scene();
            Save();

            Container.Bind<Session>()
                .FromInstance(_settings.Session)
                .AsSingle();

            Container.BindInterfacesAndSelfTo<DoTweenService>()
                .AsSingle();
        }

        
        /// <summary>
        /// Bind the save objects and events
        /// </summary>
        public void Save()
        {
            Container.DeclareSignal<SaveSignal>();
            Container.DeclareSignal<DeleteSignal>();
            Container.DeclareSignal<SaveModelSignal>();
            
            Container.BindInterfacesAndSelfTo<FileSaveService>()
                .AsSingle();

            Container.BindInterfacesAndSelfTo<SaveController>()
                .AsSingle()
                .WithArguments(_saveSettings)
                .NonLazy();
            
            Container.BindSignal<SaveSignal>()
                .ToMethod<SaveController>(x => x.OnSaveSignal)
                .FromResolve();
            
            Container.BindSignal<SaveModelSignal>()
                .ToMethod<SaveController>(x => x.OnSaveModelSignal)
                .FromResolve();
            
            Container.BindSignal<DeleteSignal>()
                .ToMethod<SaveController>(x => x.OnDeleteSignal)
                .FromResolve();
        }

        
        /// <summary>
        /// Bind scene objects and events
        /// </summary>
        public void Scene()
        {
            Container.DeclareSignal<LoadSceneSignal>();
            Container.DeclareSignal<PlaySignal>();
            Container.DeclareSignal<SceneLoadedSignal>();
            
            Container.BindInterfacesAndSelfTo<SceneController>()
                .AsSingle()
                .NonLazy();
            
            Container.BindSignal<TransitionVisibleSignal>()
                .ToMethod<SceneController>(x => x.OnTransitionVisible)
                .FromResolve();
            
            Container.BindSignal<LoadSceneSignal>()
                .ToMethod<SceneController>(x => x.OnLoadScene)
                .FromResolve();
            
            Container.BindSignal<PlaySignal>()
                .ToMethod<SceneController>(x => x.OnPlay)
                .FromResolve();
        }
        
        
        [Serializable]
        public class Settings
        {
            public Session Session;
        }
    }
}