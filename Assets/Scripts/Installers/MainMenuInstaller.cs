using Zenject;
using AB.Controllers;
using AB.Events;

namespace AB.Installers
{
    public class MainMenuInstaller : MonoInstaller
    {
        public override void InstallBindings()
        {
            Container.BindSignal<SceneLoadedSignal>()
                .ToMethod<MainMenuController>(x => x.OnSceneLoaded)
                .FromResolve();
        }
    }
}