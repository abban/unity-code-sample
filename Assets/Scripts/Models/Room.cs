﻿using UnityEngine;

namespace AB.Models
{
	[CreateAssetMenu(fileName = "Room", menuName = "AB/Models/Room")]
	public class Room : ScriptableObject
	{
		public string SceneName;
	}
}
