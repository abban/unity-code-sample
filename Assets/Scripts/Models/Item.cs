﻿using UnityEngine;
using AB.Level.InventorySystem;

namespace AB.Models
{
	[CreateAssetMenu(fileName = "Item", menuName = "AB/Models/Item")]
	public class Item : ScriptableObject
	{
		public string Key;
		public string Name;
		public GameObject WorldItemPrefab;
		public GuiItem GuiItemPrefab;
	}
}
