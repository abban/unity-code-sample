﻿using System.Collections.Generic;
using UnityEngine;

namespace AB.Models
{	
	[CreateAssetMenu(fileName = "SaveModel", menuName = "AB/Models/Save Model")]
	public class SaveModel : ScriptableObject
	{
		public string Key;
		public List<Item> Items;
		
		[SerializeField] private string _defaultRoom;
		
		private string _currentRoom;
		public string CurrentRoom
		{
			get { return _currentRoom != string.Empty ? _currentRoom : _defaultRoom; }
			private set { _currentRoom = value; }
		}
		
		
		/// <summary>
		/// Reset this object to defaults
		/// </summary>
		public void Reset()
		{
			CurrentRoom = _defaultRoom;
			Items = new List<Item>();
		}
	}
}
