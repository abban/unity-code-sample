﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace AB.Models
{
	[CreateAssetMenu(fileName = "Session", menuName = "AB/Models/Session")]
	public class Session : ScriptableObject
	{
		public SaveModel CurrentSave;
		public List<Item> Inventory => CurrentSave.Items;
		
		
		/// <summary>
		/// Check an item exists in the inventory
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public bool HasItem(string key)
		{
			return Inventory.Any(x => x.Key == key);
		}

		
		/// <summary>
		/// Check an item exists in the inventory
		/// </summary>
		/// <param name="item"></param>
		/// <returns></returns>
		public bool HasItem(Item item)
		{
			return Inventory.Contains(item);
		}

		
		/// <summary>
		/// Add an item to the inventory
		/// </summary>
		/// <param name="item"></param>
		public void AddItem(Item item)
		{
			if (HasItem(item.Key)) return;
			
			Inventory.Add(item);
		}

		
		/// <summary>
		/// Get an item from the inventory
		/// </summary>
		/// <param name="key"></param>
		/// <returns></returns>
		public Item GetItem(string key)
		{
			return Inventory.FirstOrDefault(x => x.Key == key);
		}


		/// <summary>
		/// Remove items from the inventory
		/// </summary>
		/// <param name="key"></param>
		public void RemoveItem(string key)
		{
			Inventory.RemoveAll(x => x.Key == key);
		}
		
		
		/// <summary>
		/// Remove items from the inventory
		/// </summary>
		/// <param name="item"></param>
		public void RemoveItem(Item item)
		{
			Inventory.Remove(item);
		}
	}
}