﻿using UnityEngine;
using AB.Models;

namespace AB.Level
{
	public class WorldItem : MonoBehaviour
	{
		public Item Item;

		public void Kill()
		{
			Destroy(gameObject);
		}
	}
}