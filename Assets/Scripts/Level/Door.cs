﻿using UnityEngine;
using AB.Models;

namespace AB.Level
{
	public class Door : MonoBehaviour
	{
		public Room Room;
		public Item RequiredItem;
	}
}