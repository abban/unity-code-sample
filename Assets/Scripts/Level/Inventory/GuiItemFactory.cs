﻿using UnityEngine;
using Zenject;
using AB.Models;

namespace AB.Level.InventorySystem
{
	public class GuiItemFactory : IFactory<Item, Transform, GuiItem>
	{
		private readonly DiContainer _container;

		public GuiItemFactory(DiContainer container)
		{
			_container = container;
		}

		public GuiItem Create(Item item, Transform container)
		{
			return _container.InstantiatePrefabForComponent<GuiItem>(item.GuiItemPrefab, container);
		}
	}
}