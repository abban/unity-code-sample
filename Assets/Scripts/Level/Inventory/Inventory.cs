﻿using AB.Models;
using UnityEngine;
using Zenject;

namespace AB.Level.InventorySystem
{
    public class Inventory : IInitializable
    {
        private Session _session;
        private readonly InventoryView _inventoryView;


        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="session"></param>
        /// <param name="inventoryView"></param>
        public Inventory(
            Session session,
            InventoryView inventoryView)
        {
            _session = session;
            _inventoryView = inventoryView;
        }
        
        
        /// <summary>
        /// Initialise the view
        /// </summary>
        public void Initialize()
        {
            foreach (var item in _session.Inventory)
            {
                _inventoryView.OnAdded(item);
            }
            _inventoryView.Sort(_session.Inventory);
        }
        
        
        /// <summary>
        /// Add an item to the inventory
        /// </summary>
        /// <param name="item"></param>
        public void AddItem(Item item)
        {
            if (HasItem(item)) return;
            
            _session.AddItem(item);
            _inventoryView.OnAdded(item);
            _inventoryView.Sort(_session.Inventory);
        }

        
        /// <summary>
        /// Remove an item from the inventory
        /// </summary>
        /// <param name="itemId"></param>
        public void RemoveItem(string itemId)
        {
            RemoveItem(GetItem(itemId));
        }

        
        /// <summary>
        /// Remove an item from the inventory
        /// </summary>
        /// <param name="item"></param>
        public void RemoveItem(Item item)
        {
            if (!HasItem(item)) return;
            
            _session.RemoveItem(item.Key);
            _inventoryView.OnRemoved(item);
            _inventoryView.Sort(_session.Inventory);
        }

        
        /// <summary>
        /// Get an item from the inventory
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        public Item GetItem(string itemId)
        {
            return _session.GetItem(itemId);
        }

        
        /// <summary>
        /// If an item is in the inventory
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public bool HasItem(Item item)
        {
            return _session.HasItem(item);
        }
    }
}