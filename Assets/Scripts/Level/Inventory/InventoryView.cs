﻿using System.Collections.Generic;
using UnityEngine;
using Zenject;
using AB.Models;

namespace AB.Level.InventorySystem
{
	public class InventoryView : MonoBehaviour
	{
		public Transform[] Slots;

		private GuiItem.Factory _guiItemFactory;
		
		private Dictionary<string, GuiItem> _currentItems;
		
		
		/// <summary>
		/// Constructor
		/// </summary>
		/// <param name="guiItemFactory"></param>
		[Inject]
		public void Construct(GuiItem.Factory guiItemFactory)
		{
			_guiItemFactory = guiItemFactory;
		}

		
		/// <summary>
		/// Use this for initialisation
		/// </summary>
		public void Start()
		{
			_currentItems = new Dictionary<string, GuiItem>();
		}

		
		/// <summary>
		/// When an item is added
		/// </summary>
		public void OnAdded(Item item)
		{	
			if (_currentItems.Count >= Slots.Length)
			{
				Debug.LogError("All slots are full");
				return;
			}
			
			_currentItems.Add(
				item.Key,
				_guiItemFactory.Create(item, Slots[_currentItems.Count])
			);
		}
		
		
		/// <summary>
		/// When an item is removed
		/// </summary>
		public void OnRemoved(Item item)
		{
			var guiItem = _currentItems[item.Key];
			if (guiItem == null) return;
			
			Destroy(guiItem.gameObject);
			_currentItems.Remove(item.Key);
		}
		
		
		/// <summary>
		/// Sort the items and create the missing ones
		/// </summary>
		/// <param name="items"></param>
		public void Sort(IEnumerable<Item> items)
		{
			var i = 0;
			foreach (var item in items)
			{
				_currentItems[item.Key].transform.SetParent(Slots[i]);
				i++;
			}
		}
	}
}